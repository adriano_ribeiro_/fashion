/**
 *
 */
var brand = {

    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#modalDetails',
    updateModal: '#updateModal',
    updateBtn: '#btn-update',

    loadbrandModal: null,
    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        name: null,
        nameFantasy: null,
        phone: null,
    },

    index: function () {
        brand.validador();
        brand.loadTable();
        brand.btnUpdate();
        $('#name').focus();
    },

    // salvando cor
    save: function () {
        this.data = $(brand.formCreate).serialize();
        $.post(uri.brand.create, this.data, function (response) {
            console.log(response);
            tool.response(response, 'Marca', function(){
                toastr.success('Marca '+ response.data +' cadastrada com sucesso', 'Informação');
                $(brand.formCreate).trigger("reset");
                brand.reloadTable();
            });
        });
    },

    // Preenche os campos pelo os id's dos inputs que fica no modal
    detailsUpdate: function (id, name, nameFantasy, phone) {
        $('#update-id').val(id);
        $('#update-name').val(name);
        $('#update-nameFantasy').val(nameFantasy);
        $('#update-phone').val(phone);
    },

    // Preencher modal para edição da marca pelo o id
    modalUpdate: function (id) {
        $.get(uri.brand.details + id, function (response) {
            tool.response(response, 'Cor ', function () {
                $(brand.updateModal).modal('show');
                brand.params.id = response.data.id;
                brand.params.name = response.data.name;
                brand.params.nameFantasy = response.data.nameFantasy;
                brand.params.phone = response.data.phone;
                brand.detailsUpdate(brand.params.id, brand.params.name, brand.params.nameFantasy, brand.params.phone);
            });
        });
    },

    // Aqui vc edita uma marca
    update: function (name, nameFantasy, phone) {
        this.data = {
            'name': name,
            'nameFantasy': nameFantasy,
            'phone': phone
        };

        $.post(uri.brand.update + brand.params.id, this.data, function (response) {
            console.log(response);
            tool.response(response, 'Marca ', function () {
                toastr.success('Marca ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                $('#update-name').val('');
                $('#update-nameFantasy').val('');
                $('#update-phone').val('');
                $(brand.updateModal).modal('hide');
                brand.reloadTable();
            });
        });
    },

    // botão que faz ação para editar uma marca.
    btnUpdate: function () {
        $(brand.updateBtn).click(function () {
            brand.params.name = $('#update-name').val();
            brand.params.nameFantasy = $('#update-nameFantasy').val();
            brand.params.phone = $('#update-phone').val();
            if (brand.params.name !== '' || brand.params.name !== null ||
                brand.params.nameFantasy !== '' || brand.params.nameFantasy !== null ||
                brand.params.phone !== '' || brand.params.phone !== null
            ) {
                return brand.update(
                    brand.params.name,
                    brand.params.nameFantasy,
                    brand.params.phone
                );
            }
            return toastr.warning('Verifique todos os campos', 'Advertência');
        });
    },

    // Aqui vc valida os dados do formulário antes de salvar uma marca
    validador: function () {
        $(brand.formCreate).validate({
            rules: {
                name: {
                    required: true
                },
                nameFantasy: {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: 'Campo nome obrigatório'
                },
                nameFantasy: {
                    required: 'Campo nome fantasia obrigatório'
                },
                phone: {
                    required: 'Campo telefone obrigatório'
                }
            },
            submitHandler: function () {
                brand.save();
                return false;
            }
        });
    },

    loadTable: function () {
        brand.active = $('#brands').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.brand.all,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "name"},
                {"data": "nameFantasy"},
                {"data": "phone"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-info btn-sm fa fa-edit'> Editar</button>";
                    },
                    targets: 3,
                    "searchable": false
                },

                {orderable: false, targets: [0]}
            ]
        });

        brand.active.on('click', '.update', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            brand.modalUpdate(id);
            $(brand.updateModal).modal({backdrop: 'static', keybrand: false});
        });
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#brandActive').DataTable().ajax.reload();
    reloadTable: function () {
        brand.active.ajax.reload();
        // brand.inactive.ajax.reload();
    },
};
