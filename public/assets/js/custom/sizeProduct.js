/**
 * sizeProduct.js
 */
var sizeProduct = {

    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#modalDetails',
    updateModal: '#updateModal',
    updateBtn: '#btn-update',

    loadColorModal: null,
    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        size: null,
    },

    index: function () {
        sizeProduct.validador();
        sizeProduct.loadTable();
        sizeProduct.btnUpdate();
        $('#size').focus();
    },

    // salvando cor
    save: function () {
        this.data = $(sizeProduct.formCreate).serialize();
        $.post(uri.sizeProduct.create, this.data, function (response) {
            console.log(response);
            tool.response(response, 'Tamanho', function(){
                toastr.success('Novo tamho cadastrado com sucesso', 'Informação');
                $(sizeProduct.formCreate).trigger("reset");
                sizeProduct.reloadTable();
            });
        });
    },

    // Preenche os campos pelo os id's dos inputs que fica no modal
    detailsUpdate: function (id, size) {
        $('#update-id').val(id);
        $('#update-size').val(size);
    },

    // Preencher modal para edição da cor pelo o id
    modalUpdate: function (id) {
        $.get(uri.sizeProduct.details + id, function (response) {
            tool.response(response, 'Tamanho ', function () {
                $(sizeProduct.updateModal).modal('show');
                console.log(response);
                sizeProduct.params.id = response.data.id;
                sizeProduct.params.size = response.data.size;
                sizeProduct.detailsUpdate(sizeProduct.params.id, sizeProduct.params.size);
            });
        });
    },

    // Aqui vc edita uma cor
    update: function (size) {
        this.data = {
            'size': size
        };

        $.post(uri.sizeProduct.update + sizeProduct.params.id, this.data, function (response) {
            tool.response(response, 'Tamanho ', function () {
                toastr.success('Tamanho ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                $('#update-size').val('');
                $(sizeProduct.updateModal).modal('hide');
                sizeProduct.reloadTable();
            });
        });
    },

    // botão que faz ação para editar uma cor.
    btnUpdate: function () {
        $(sizeProduct.updateBtn).click(function () {
            sizeProduct.params.size = $('#update-size').val();
            if (sizeProduct.params.size !== '' || sizeProduct.params.size !== null) {
                return sizeProduct.update(sizeProduct.params.size);
            }
            return toastr.warning('Verifique todos os campos', 'Advertência');
        });
    },

    // Aqui vc valida os dados do formulário antes de salvar uma cor
    validador: function () {
        $(sizeProduct.formCreate).validate({
            rules: {
                size: {
                    required: true
                },
            },
            messages: {
                size: {
                    required: 'Tamanho obrigatório'
                }
            },
            submitHandler: function () {
                sizeProduct.save();
                return false;
            }
        });
    },

    loadTable: function () {
        sizeProduct.active = $('#sizeProduct').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.sizeProduct.all,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "size"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-info btn-sm fa fa-edit'> Editar</button>";
                    },
                    targets: 1,
                    "searchable": false
                },

                {orderable: false, targets: [1]}
            ]
        });

        sizeProduct.active.on('click', '.update', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            sizeProduct.modalUpdate(id);
            $(sizeProduct.updateModal).modal({backdrop: 'static', keycolor: false});
        });
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#colorActive').DataTable().ajax.reload();
    reloadTable: function () {
        sizeProduct.active.ajax.reload();
        // color.inactive.ajax.reload();
    },
};
