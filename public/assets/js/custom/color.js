/**
 *
 */
var color = {

    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#modalDetails',
    updateModal: '#updateModal',
    updateBtn: '#btn-update',

    loadColorModal: null,
    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        description: null,
    },

    index: function () {
        color.validador();
        color.loadTable();
        color.btnUpdate();
        $('#description').focus();
    },

    // salvando cor
    save: function () {
        this.data = $(color.formCreate).serialize();
        $.post(uri.color.create, this.data, function (response) {
            console.log(response);
            tool.response(response, 'Cor', function(){
                toastr.success('Nova cor cadastrada com sucesso', 'Informação');
                $(color.formCreate).trigger("reset");
                color.reloadTable();
            });
        });
    },

    // Preenche os campos pelo os id's dos inputs que fica no modal
    detailsUpdate: function (id, description) {
        $('#update-id').val(id);
        $('#update-description').val(description);
    },

    // Preencher modal para edição da cor pelo o id
    modalUpdate: function (id) {
        $.get(uri.color.details + id, function (response) {
            tool.response(response, 'Cor ', function () {
                $(color.updateModal).modal('show');
                console.log(response);
                color.params.id = response.data.id;
                color.params.description = response.data.description;
                color.detailsUpdate(color.params.id, color.params.description);
            });
        });
    },

    // Aqui vc edita uma cor
    update: function (description) {
        this.data = {
            'description': description
        };

        $.post(uri.color.update + color.params.id, this.data, function (response) {
            tool.response(response, 'Cor ', function () {
                toastr.success('Cor ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                $('#update-description').val('');
                $(color.updateModal).modal('hide');
                color.reloadTable();
            });
        });
    },

    // botão que faz ação para editar uma cor.
    btnUpdate: function () {
        $(color.updateBtn).click(function () {
            color.params.description = $('#update-description').val();
            if (color.params.description !== '' || color.params.description !== null) {
                return color.update(color.params.description);
            }
            return toastr.warning('Verifique todos os campos', 'Advertência');
        });
    },

    // Aqui vc valida os dados do formulário antes de salvar uma cor
    validador: function () {
        $(color.formCreate).validate({
            rules: {
                description: {
                    required: true
                },
            },
            messages: {
                description: {
                    required: 'Campo descrição da cor obrigatório'
                }
            },
            submitHandler: function () {
                color.save();
                return false;
            }
        });
    },

    loadTable: function () {
        color.active = $('#colors').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.color.all,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "description"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-info btn-sm fa fa-edit'> Editar</button>";
                    },
                    targets: 1,
                    "searchable": false
                },

                {orderable: false, targets: [1]}
            ]
        });

        color.active.on('click', '.update', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            color.modalUpdate(id);
            $(color.updateModal).modal({backdrop: 'static', keycolor: false});
        });
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#colorActive').DataTable().ajax.reload();
    reloadTable: function () {
        color.active.ajax.reload();
        // color.inactive.ajax.reload();
    },
};
