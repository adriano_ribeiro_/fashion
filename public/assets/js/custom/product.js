/**
 * product.js
 */
var product = {

    brand: '#brand',
    sizeProduct: '#sizeProduct',
    color: '#color',

    formCreate: '#formCreate',
    formUpdate: '#formUpdate',
    modalDetails: '#modalDetails',
    updateModal: '#updateModal',
    updateBtn: '#btn-update',

    loadProductModal: null,
    active: null,
    inactive: null,
    data: null,

    params: {
        id: null,
        description: null,
    },

    index: function () {
        product.selectBrand();
        product.selectSizeProduct();
        product.selectColor();
        product.validador();
        product.loadTable();
        product.btnUpdate();
        $('#description').focus();

        $("#photoProduct").fileinput({
            language: "pt-BR",
            maxFileCount: 10,
            // theme: 'fas',
            'theme': 'explorer-fas',
            allowedFileTypes: ["image"]
        });

        tool.numberPrototypeFormatMoney();
    },

    // salvando produto
    save: function () {
        let data = new FormData();
        data.append('description', $('#description').val());
        data.append('reference', $('#reference').val());
        data.append('quantity', $('#quantity').val());
        data.append('priceBuy', $('#priceBuy').val());
        data.append('priceSale', $('#priceSale').val());
        data.append('brand', $('#brand').val());
        data.append('sizeProduct', $('#sizeProduct').val());
        data.append('color', $('#color').val());

        $.each($('#photoProduct').prop('files'), function (key, value) {
            data.append('files[]', $('#photoProduct').prop('files')[key]);
        });

        $.ajax({
            url : uri.product.create,
            type : 'POST',
            data : data,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success : function(response) {
                toastr.success('Produto ' + response.data + ' cadastrado com sucesso!', 'Cadastrando . . .');
                $(product.formCreate).trigger("reset");
                product.reloadTable();
            },
            error: function(response) {
                toastr.error(response.data, 'Erro');
            }
        });
    },

    // Preenche os campos pelo os id's dos inputs que fica no modal
    detailsUpdate: function (id, description) {
        $('#update-id').val(id);
        $('#update-description').val(description);
    },

    // Preencher modal para edição da produto pelo o id
    modalUpdate: function (id) {
        $.get(uri.product.details + id, function (response) {
            console.log(response);
            tool.response(response, 'Produto ', function () {
                $(product.updateModal).modal('show');
                product.params.id = response.data.id;
                product.params.description = response.data.description;
                product.detailsUpdate(product.params.id, product.params.description);
            });
        });
    },

    // Aqui vc edita uma produto
    update: function (size) {
        this.data = {
            'size': size
        };

        $.post(uri.product.update + product.params.id, this.data, function (response) {
            tool.response(response, 'Produto ', function () {
                toastr.success('Produto ' + response.data + ' atualizado com sucesso!', 'Atualizando . . .');
                $('#update-size').val('');
                $(product.updateModal).modal('hide');
                product.reloadTable();
            });
        });
    },

    // botão que faz ação para editar uma produto.
    btnUpdate: function () {
        $(product.updateBtn).click(function () {
            product.params.description = $('#update-description').val();
            if (product.params.description !== '' || product.params.description !== null) {
                return product.update(product.params.description);
            }
            return toastr.warning('Verifique todos os campos', 'Advertência');
        });
    },

    // Aqui vc valida os dados do formulário antes de salvar uma produto
    validador: function () {
        $(product.formCreate).validate({
            rules: {
                description: {
                    required: true
                },
            },
            messages: {
                description: {
                    required: 'Campo descrição obrigatório'
                }
            },
            submitHandler: function () {
                product.save();
                return false;
            }
        });
    },

    loadTable: function () {
        product.active = $('#products').DataTable({
            "language": {
                "url": "/assets/Portuguese-Brasil.json"
            },
            "ajax": {
                "url": uri.product.all,
                "type": "GET",
                "dataSrc": "data"
            },
            "columns": [
                {"data": "description"},
                {"data": "reference"},
                {"data": "priceSale"},
                {"data": "quantity"},
                {"data": "brand"},
                {"data": "id"},
                {"data": "id"}
            ],
            columnDefs: [
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-warning btn-sm fa fa-info-circle'> Detalhes</button>";
                    },
                    targets: 5,
                    "searchable": false
                },
                {
                    render: function (data) {
                        return "<button href= '" + data + "' class='update btn-info btn-sm fa fa-edit'> Editar</button>";
                    },
                    targets: 6,
                    "searchable": false
                },
                {
                    render: (data) => {
                        data = parseFloat(data);
                        return 'R$ ' + data.formatMoney(2, ',', '.');
                    },
                    targets: [2]
                },

                {orderable: false, targets: [0]}
            ]
        });

        product.active.on('click', '.update', function (e) {
            e.preventDefault();
            const id = $(this).attr('href');
            product.modalUpdate(id);
            $(product.updateModal).modal({backdrop: 'static', keycolor: false});
        });

        tool.numberPrototypeFormatMoney();
    },

    // Refresh na página sem recarregar
    // É a mesma coisa que :  $('#colorActive').DataTable().ajax.reload();
    reloadTable: function () {
        product.active.ajax.reload();
        // color.inactive.ajax.reload();
    },

    selectBrand: function () {
        $(product.brand).select2({
            language: 'pt-BR',
            allowClear: true,
            placeholder: 'Selecione a marca',
            ajax: {
                url: uri.brand.select,
                dataType: 'json',
                delay: 250,
                processResults: function (response) {
                    return {
                        results: response.data
                    };
                },
                cache: true
            }
        });
    },

    selectSizeProduct: function () {
        $(product.sizeProduct).select2({
            language: 'pt-BR',
            allowClear: true,
            placeholder: 'Selecione o tamanho',
            ajax: {
                url: uri.sizeProduct.select,
                dataType: 'json',
                delay: 250,
                processResults: function (response) {
                    return {
                        results: response.data
                    };
                },
                cache: true
            }
        });
    },

    selectColor: function () {
        $(product.color).select2({
            language: 'pt-BR',
            allowClear: true,
            placeholder: 'Selecione a cor',
            ajax: {
                url: uri.color.select,
                dataType: 'json',
                delay: 250,
                processResults: function (response) {
                    return {
                        results: response.data
                    };
                },
                cache: true
            }
        });
    }
};
