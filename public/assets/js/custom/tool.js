var tool = {

    ajax: function (url, data, type, callback) {
        $.ajax({
            'url': url,
            'type': type,
            'data': data,
            'dataType': 'json',
            success: function (response) {
                callback(response);
            },
            error: function (response) {
                callback(response);
            }
        });
    },

    messageToast: function (type, message) {

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        toastr[type](message);
    },

    resetForm: function (form) {
        $(form)[0].reset();
    },

    inputFocus: function (input) {
        $('#' + input).focus();
    },

    addValidationCpf: function () {
        jQuery.validator.addMethod("cpf", function (value, element) {
            value = jQuery.trim(value);

            value = value.replace('.', '');
            value = value.replace('.', '');
            cpf = value.replace('-', '');
            while (cpf.length < 11) cpf = "0" + cpf;
            let expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
            let a = [];
            let b = new Number;
            let c = 11;
            for (i = 0; i < 11; i++) {
                a[i] = cpf.charAt(i);
                if (i < 9) b += (a[i] * --c);
            }
            if ((x = b % 11) < 2) {
                a[9] = 0
            } else {
                a[9] = 11 - x
            }
            b = 0;
            c = 11;
            for (y = 0; y < 10; y++) b += (a[y] * c--);
            if ((x = b % 11) < 2) {
                a[10] = 0;
            } else {
                a[10] = 11 - x;
            }

            let retorno = true;
            if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;

            return this.optional(element) || retorno;

        }, "INFORME UM CPF CORRETO");
    },

    valideEmail: function (email) {
        er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
        if (er.exec(email))
            return true;
        else
            return false;
    },

    iscpf: function (cpfValue) {
        cpfValue = cpfValue.replace(/[^\d]+/g, '');
        if (cpfValue == '') return false;
        // Elimina cpfValues invalidos conhecidos
        if (cpfValue.length != 11 ||
            cpfValue == "00000000000" ||
            cpfValue == "11111111111" ||
            cpfValue == "22222222222" ||
            cpfValue == "33333333333" ||
            cpfValue == "44444444444" ||
            cpfValue == "55555555555" ||
            cpfValue == "66666666666" ||
            cpfValue == "77777777777" ||
            cpfValue == "88888888888" ||
            cpfValue == "99999999999")
            return false;
        // Valida 1o digito
        add = 0;
        for (i = 0; i < 9; i++)
            add += parseInt(cpfValue.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpfValue.charAt(9)))
            return false;
        // Valida 2o digito
        add = 0;
        for (i = 0; i < 10; i++)
            add += parseInt(cpfValue.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpfValue.charAt(10)))
            return false;
        return true;
    },

    isCNPJValid: function (cnpjParam) {
        cnpjParam = cnpjParam.replace(/[^\d]+/g, '');
        if (cnpjParam == '')
            return false;
        if (cnpjParam.length != 14)
            return false;
        if (cnpjParam == "00000000000000" || cnpjParam == "11111111111111"
            || cnpjParam == "22222222222222" || cnpjParam == "33333333333333"
            || cnpjParam == "44444444444444" || cnpjParam == "55555555555555"
            || cnpjParam == "66666666666666" || cnpjParam == "77777777777777"
            || cnpjParam == "88888888888888" || cnpjParam == "99999999999999")
            return false;

        tamanho = cnpjParam.length - 2
        numeros = cnpjParam.substring(0, tamanho);
        digitos = cnpjParam.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;

        tamanho = tamanho + 1;
        numeros = cnpjParam.substring(0, tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--) {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;

        return true;
    },

    validationCNPJ: function () {
        jQuery.validator.addMethod("cnpj", function (cnpj, element) {
            return tool.isCNPJValid(cnpj);
        }, "INFORME UM CNPJ CORRETO.");
    },

    setValueOptionSelect2: function (value, select) {
        if (value !== 0) {
            setTimeout(function () {
                $('#' + select).val(value).change();
            }, 1000);
        }
    },

    setValueOptionSelect2ByText: function (value, input_id) {
        $('#' + input_id).val($('option:contains("' + value + '")').val());
    },

    setTitle: function (title) {
        $('#title-page').text(title);
    },

    response: function (response, title, callback) {
        if (response.code === true) {
            callback();
        } else if (response.code === 1) {
            toastr.info(title + ' ' + response.data, 'Informação');
        } else if (response.code === 2) {
            toastr.warning(response.data, 'Advertência');
        } else if (response.code === false) {
            toastr.error(response.data, 'Erro');
        } else {
            toastr.error(response.data, 'Error');
        }
    },

    scrollDiv: function (element) {
        $('html, body').animate({scrollTop: $('.' + element).position().top}, 'slow');
    },

    clickSpanScroll: function () {
        $('.group-button span').click(function () {
            let value = $(this).attr('id');
            console.log(value);
            tool.scrollDiv(value);
        });

        $('.group-button-footer span').click(function () {
            let value = $(this).attr('id');
            tool.scrollDiv(value);
        });
    },

    hide: function (select) {
        $('#' + select).hide();
    },

    numberPrototypeFormatMoney: function() {
        Number.prototype.formatMoney = function (c, d, t) {
            var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d,
                t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
                j = (j = i.length) > 3 ? j % 3 : 0;
            return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
        };
    },

    convertMoedaToFloat: function (valor) {
        if (valor === "" || valor === undefined) {
            valor = null;
        } else {
            valor = valor.replace(".", "");
            valor = valor.replace(",", ".");
            valor = parseFloat(valor);
        }
        return valor;
    },

    money: function () {
        $(".money").maskMoney({
            prefix: "R$ ",
            decimal: ",",
            thousands: "."
        });
    },

    enabled: function (target) {
        $("#" + target).prop("disabled", false);
    },

    disabled: function (target) {
        $("#" + target).prop("disabled", true);
    },

    convertDateBr: (data) => {
        return moment(new Date(data)).format('DD/MM/YYYY HH:mm:ss')
        // return moment(data).utc().format("DD/MM/YYYY");
    },

    convertDateBrToDefault: function () {

        var start = $('#start_date').val();
        var end = $('#end_date').val();
        // start = start.split('-');
        // end = end.split('-');

        //var vname = $("#EarningsTypes").val() == undefined ? '' : $("#EarningsTypes").val().trim();

        var start_time = '00:00:00';
        var end_time = '23:59:59';
        var start_date = start === undefined ? '' : start.trim();
        var end_date = end === undefined ? '' : end.trim();

        start_date = formartDate(start_date);
        end_date = formartDate(end_date);

        start_date = start_date + " " + start_time;
        end_date = end_date + " " + end_time;

        return {'start': start_date, 'end': end_date};

        function formartDate (date) {
            date = date.split('/');
            var dia = start;
            var mes = date[1];
            var ano = date[2];

            return ano + '-' + mes + '-' + dia;
        }
    },

    formatDateBr: function (dateObject) {
        let y = dateObject.split('-');
        let dia = y[2];
        let mes = y[1];
        let ano = y[0];
        return dia + "/" + mes + "/" + ano;
    },

    formatDateHour: function (data) {
        let date = data;
        let aux = tool.formatDateBr(date.split(' ')[0]);
        let h = date.split(' ')[1];
        date = aux.toString() + ' ' + h.toString();
        return date;
    },

    getCountArrayByElement: function (array, value) {
        let keys = Object.keys(array);
        let response = keys.filter(function (v) {
            return array[v].description === value;
        });
        return response.length;
    },

    calendar: function () {
        $('.date_picker').datepicker({
            autoClose: true,
            dateFormat: 'dd/mm/yyyy'
        });
    },

    comanda: function (id) {
        let w = 500;
        let h = 500;
        let left = (screen.width / 2) - (w / 2);
        let top = (screen.height / 2) - (h / 2);
        window.open("/venda/comanda/" + id, "Comanda", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    },

    report: function (start, end) {
        let w = 500;
        let h = 500;
        let left = (screen.width / 2) - (w / 2);
        let top = (screen.height / 2) - (h / 2);
        window.open("/venda/relatorio/"+start+"/"+end, "Comanda", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    }
};