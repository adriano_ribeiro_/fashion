/**
 * uri das rotas
 */
var uri = {

    color: {
        create: '/color/create',
        details: '/color/details/',
        update: '/color/update/',
        all: '/color/all',
        select: '/color/select',
    },

    brand: {
        create: '/brand/create',
        details: '/brand/details/',
        update: '/brand/update/',
        all: '/brand/all',
        select: '/brand/select',
    },

    sizeProduct: {
        create: '/size/product/create',
        details: '/size/product/details/',
        update: '/size/product/update/',
        all: '/size/product/all',
        select: '/size/select',
    },

    product: {
        create: '/product/create',
        details: '/product/details/',
        update: '/product/update/',
        all: '/product/all',
    }
};