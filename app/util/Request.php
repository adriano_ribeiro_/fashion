<?php

namespace app\util;

use app\exception\ValidationException;

/**
 * Class Request
 * @package app\util
 */
class Request
{
    /**
     * @param $type
     * @return bool
     * @throws ValidationException
     */
    public static function request($type)
    {
        if ($_SERVER['REQUEST_METHOD'] != strtoupper($type)) {
            throw new ValidationException("A requisição não pode ser do tipo: <b>{$type}</b>", 9999);
        }
        return true;
    }
}