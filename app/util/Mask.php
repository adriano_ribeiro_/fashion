<?php

namespace app\util;

/**
 * Class Mask
 * @package app\util
 */
abstract class Mask
{
    /**
     * @param string $value
     * @return float
     */
    public static function convertStringToFloat(string $value)
    {
        $value = str_replace('.', '', $value);
        $value = str_replace(',', '.', $value);
        return floatval(preg_replace("/[^-0-9\.]/","",$value));
    }
}