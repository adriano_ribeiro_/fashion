<?php

namespace app\util;

use app\model\User;
use Aura\Session\Session;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Auth
 * @package app\util
 */
class Auth
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * @var string $email
     *
     * @Assert\NotBlank(message="Campo email obrigatório")
     *
     * @Assert\Email(message="email inválido")
     */
    private $email;


    /**
     * @var string $password
     *
     * @Assert\NotBlank(message="Campo senha obrigatório")
     *
     */
    private $password;

    /**
     * Auth constructor.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Auth
     */
    public function setEmail(string $email): Auth
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Auth
     */
    public function setPassword(string $password): Auth
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param User $user
     */
    public function generateUserSession(User $user)
    {
        $this->session->getSegment('Logged')->set('user', [
            "id" => $user->getId(),
            "name" => $user->getPerson()->getName(),
            "email" => $user->getEmail()
        ]);
    }
}