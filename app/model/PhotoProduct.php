<?php

namespace app\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PhotoProduct - Foto Produto
 *
 * @ORM\Table(name="photos_products")
 * @ORM\Entity(repositoryClass="app\repository\PhotoProduct\PhotoProductRepository")
 */
class PhotoProduct extends Model
{
    /**
     * @var string $base64
     *
     * @ORM\Column(name="base64", type="text", nullable=true)
     * @Assert\NotBlank(message="Campo base64 obrigatório")
     */
    private $base64;

    /**
     * @var Product - Produto
     *
     * @ORM\ManyToOne(targetEntity="app\model\Product", inversedBy="photoProduct")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=true)
     * @Assert\NotBlank(message="Campo produto obrigatório")
     */
    private $product;

    /**
     * @return string|null
     */
    public function getBase64(): ?string
    {
        return $this->base64;
    }

    /**
     * @param string $base64
     * @return PhotoProduct
     */
    public function setBase64(string $base64): PhotoProduct
    {
        $this->base64 = $base64;
        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return PhotoProduct
     */
    public function setProduct(Product $product): PhotoProduct
    {
        $this->product = $product;
        return $this;
    }
}