<?php

namespace app\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Person - Pessoa
 *
 * @ORM\Table(name="people")
 * @ORM\Entity(repositoryClass="app\repository\Person\PersonRepository")
 */
class Person extends Model
{
    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", unique=true, nullable=false)
     *
     * @Assert\NotBlank(message="Campo nome obrigatório")
     */
    private $name;

    /**
     * @var string|null $phone
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     *
     */
    private $phone;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Person
     */
    public function setName(string $name): Person
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return Person
     */
    public function setPhone(?string $phone): Person
    {
        $this->phone = $phone;
        return $this;
    }
}