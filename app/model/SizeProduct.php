<?php

namespace app\model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class SizeProduct - Tamanho Produto (PP, P, M, G, GG, XG - 36, 38, 40, 42, 44, 46, 48, 50)
 *
 * @ORM\Table(name="sizes_products")
 * @ORM\Entity(repositoryClass="app\repository\SizeProduct\SizeProductRepository")
 */
class SizeProduct extends Model
{
    /**
     * @var string $size
     *
     * @ORM\Column(name="size", type="string", nullable=false)
     */
    private $size;

    /**
     * @var Collection|null - Produto
     *
     * @ORM\OneToMany(targetEntity="app\model\Product", mappedBy="sizeProduct")
     */
    private $product;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getSize(): ?string
    {
        return $this->size;
    }

    /**
     * @param string $size
     * @return SizeProduct
     */
    public function setSize(string $size): SizeProduct
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProduct(): ?Collection
    {
        return $this->product;
    }
}