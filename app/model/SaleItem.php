<?php

namespace app\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SaleItem - Venda Item
 *
 * @ORM\Table(name="sales_items")
 * @ORM\Entity(repositoryClass="app\repository\SaleItem\SaleItemRepository")
 */
class SaleItem extends Model
{
    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="app\model\Product", inversedBy="saleItem")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo produto obrigatório")
     */
    private $product;

    /**
     * @var integer $quantity
     *
     * @ORM\Column(type="integer", name="quantity", nullable=false)
     *
     * @Assert\NotBlank(message="Campo quantidade obrigatório")
     */
    private $quantity;

    /**
     * @var float $subtotal
     *
     * @ORM\Column(type="decimal", name="subtotal", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotNull(message="Campo subtotal obrigatório")
     */
    private $subtotal;

    /**
     * @var Sale
     *
     * @ORM\ManyToOne(targetEntity="app\model\Sale", inversedBy="saleItem")
     * @ORM\JoinColumn(name="sale_id", referencedColumnName="id", nullable=false)
     *
     * @Assert\NotBlank(message="Campo venda obrigatório")
     */
    private $sale;

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return SaleItem
     */
    public function setProduct(Product $product): SaleItem
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return SaleItem
     */
    public function setQuantity(int $quantity): SaleItem
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getSubtotal(): ?float
    {
        return $this->subtotal;
    }

    /**
     * @param float $subtotal
     * @return SaleItem
     */
    public function setSubtotal(float $subtotal): SaleItem
    {
        $this->subtotal = $subtotal;
        return $this;
    }

    /**
     * @return Sale|null
     */
    public function getSale(): ?Sale
    {
        return $this->sale;
    }

    /**
     * @param Sale $sale
     * @return SaleItem
     */
    public function setSale(Sale $sale): SaleItem
    {
        $this->sale = $sale;
        return $this;
    }
}