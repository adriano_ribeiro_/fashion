<?php

namespace app\model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Client - Cliente
 *
 * @ORM\Table(name="clients")
 * @ORM\Entity(repositoryClass="app\repository\Client\ClientRepository")
 */
class Client extends Model
{
    /**
     * @var Person - Pessoa
     *
     * @ORM\OneToOne(targetEntity="app\model\Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     */
    private $person;

    /**
     * @var Collection|null - Venda
     *
     * @ORM\OneToMany(targetEntity="app\model\Sale", mappedBy="client")
     */
    private $sale;

    public function __construct()
    {
        $this->sale = new ArrayCollection();
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return Client
     */
    public function setPerson(Person $person): Client
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getSale(): ?Collection
    {
        return $this->sale;
    }
}