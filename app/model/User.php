<?php

namespace app\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User - Usuário
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="app\repository\User\UserRepository")
 */
class User extends Model
{
    /**
     * @var string $login
     *
     * @ORM\Column(name="login", type="string", unique=true, nullable=false)
     *
     * @Assert\NotBlank(message="Campo login obrigatório")
     */
    private $login;

    /**
     * @var string $password
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     * @Assert\NotBlank(message="Campo senha obrigatório")
     */
    private $password;

    /**
     * @var Person - Pessoa
     *
     * @ORM\OneToOne(targetEntity="app\model\Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     */
    private $person;

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string $login
     * @return User
     */
    public function setLogin(string $login): User
    {
        $this->login = $login;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return User
     */
    public function setPerson(Person $person): User
    {
        $this->person = $person;
        return $this;
    }
}