<?php

namespace app\model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Product - Produto
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="app\repository\Product\ProductRepository")
 */
class Product extends Model
{
    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Campo descrição obrigatório")
     */
    private $description;

    /**
     * @var string $reference
     *
     * @ORM\Column(name="reference", type="string", nullable=false)
     *
     * @Assert\NotBlank(message="Campo ref obrigatório")
     */
    private $reference;

    /**
     * @var float $priceBuy
     *
     * @ORM\Column(name="price_buy", type="decimal", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotBlank(message="Campo valor da compra obrigatório")
     */
    private $priceBuy;

    /**
     * @var float $priceSale
     *
     * @ORM\Column(name="price_sale", type="decimal", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotBlank(message="Campo valor da venda obrigatório")
     */
    private $priceSale;

    /**
     * @var float $quantity
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     *
     * @Assert\NotBlank(message="Campo quantidade obrigatório")
     */
    private $quantity;

    /**
     * @var Brand - Marca
     *
     * @ORM\ManyToOne(targetEntity="app\model\Brand", inversedBy="product")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=false)
     */
    private $brand;

    /**
     * @var SizeProduct - Tamanho (PP, P, M, G, GG, XG - 36, 38, 40, 42, 44, 46, 48, 50)
     *
     * @ORM\ManyToOne(targetEntity="app\model\SizeProduct", inversedBy="product")
     * @ORM\JoinColumn(name="size_product_id", referencedColumnName="id", nullable=false)
     */
    private $sizeProduct;

    /**
     * @var Color - Cor
     *
     * @ORM\ManyToOne(targetEntity="app\model\Color", inversedBy="product")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id", nullable=false)
     */
    private $color;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="app\model\PhotoProduct", mappedBy="product", cascade={"persist"})
     */
    private $photoProduct;

    /**
     * @var Collection|null
     *
     * @ORM\OneToMany(targetEntity="app\model\SaleItem", mappedBy="product")
     */
    private $saleItem;

    public function __construct()
    {
        $this->photoProduct = new ArrayCollection();
        $this->saleItem = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Product
     */
    public function setDescription(string $description): Product
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return Product
     */
    public function setReference(string $reference): Product
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPriceBuy(): ?float
    {
        return $this->priceBuy;
    }

    /**
     * @param float $priceBuy
     * @return Product
     */
    public function setPriceBuy(float $priceBuy): Product
    {
        $this->priceBuy = $priceBuy;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getPriceSale(): ?float
    {
        return $this->priceSale;
    }

    /**
     * @param float $priceSale
     * @return Product
     */
    public function setPriceSale(float $priceSale): Product
    {
        $this->priceSale = $priceSale;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getQuantity(): ?float
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     * @return Product
     */
    public function setQuantity(float $quantity): Product
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return Brand|null
     */
    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    /**
     * @param Brand $brand
     * @return Product
     */
    public function setBrand(Brand $brand): Product
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return SizeProduct|null
     */
    public function getSizeProduct(): ?SizeProduct
    {
        return $this->sizeProduct;
    }

    /**
     * @param SizeProduct $sizeProduct
     * @return Product
     */
    public function setSizeProduct(SizeProduct $sizeProduct): Product
    {
        $this->sizeProduct = $sizeProduct;
        return $this;
    }

    /**
     * @return Color|null
     */
    public function getColor(): ?Color
    {
        return $this->color;
    }

    /**
     * @param Color $color
     * @return Product
     */
    public function setColor(Color $color): Product
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @param PhotoProduct $photoProduct
     * @return Product
     */
    public function addPhotoProduct(PhotoProduct $photoProduct): Product
    {
        if (!$this->photoProduct->contains($photoProduct)) {
            $this->photoProduct->add($photoProduct);
            $photoProduct->setProduct($this);
        }
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getPhotoProduct(): ?Collection
    {
        return $this->photoProduct;
    }

    /**
     * @return Collection|null
     */
    public function getSaleItem(): ?Collection
    {
        return $this->saleItem;
    }
}