<?php

namespace app\model;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Business - Empresa
 *
 * @ORM\Table(name="business")
 * @ORM\Entity(repositoryClass="app\repository\Business\BusinessRepository")
 */
class Business extends Model
{
    /**
     * @var string|null $photo
     *
     * @ORM\Column(name="photo", type="string", nullable=true)
     */
    private $photo = null;

    /**
     * @var Person - Pessoa
     *
     * @ORM\OneToOne(targetEntity="app\model\Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     */
    private $person;

    /**
     * @return string|null
     */
    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    /**
     * @param string|null $photo
     * @return Business
     */
    public function setPhoto(?string $photo): Business
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return Business
     */
    public function setPerson(Person $person): Business
    {
        $this->person = $person;
        return $this;
    }
}