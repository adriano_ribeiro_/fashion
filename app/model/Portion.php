<?php

namespace app\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * Class Portion - Parcela
 *
 * @ORM\Table(name="portions")
 * @ORM\Entity(repositoryClass="app\repository\Portion\PortionRepository")
 */
class Portion extends Model
{
    /**
     * @var float $value
     *
     * @ORM\Column(name="value", type="decimal", precision=10, scale=2, nullable=false)
     * @Assert\NotBlank(message="Campo valor obrigatório")
     */
    private $value;

    /**
     * @var Sale
     *
     * @ORM\OneToOne(targetEntity="app\model\Sale", inversedBy="portion")
     * @ORM\JoinColumn(name="sale_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="Campo venda obrigatório")
     */
    private $sale;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="pay_day", type="datetime", nullable=true)
     */
    private $payDay;

    /**
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return Portion
     */
    public function setValue(float $value): Portion
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return Sale|null
     */
    public function getSale(): ?Sale
    {
        return $this->sale;
    }

    /**
     * @param Sale $sale
     * @return Portion
     */
    public function setSale(Sale $sale): Portion
    {
        $this->sale = $sale;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getPayDay(): ?DateTime
    {
        return $this->payDay;
    }

    /**
     * @param DateTime|null $payDay
     * @return Portion
     */
    public function setPayDay(?DateTime $payDay): Portion
    {
        $this->payDay = $payDay;
        return $this;
    }
}