<?php

namespace app\model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Brand - Marca
 *
 * @ORM\Table(name="brands")
 * @ORM\Entity(repositoryClass="app\repository\Brand\BrandRepository")
 */
class Brand extends Model
{
    /**
     * @var string|null $nameFantasy
     *
     * @ORM\Column(name="name_fantasy", type="string", nullable=true)
     */
    private $nameFantasy;

    /**
     * @var Person - Pessoa
     *
     * @ORM\OneToOne(targetEntity="app\model\Person")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id", nullable=false)
     */
    private $person;

    /**
     * @var Collection|null - Produto
     *
     * @ORM\OneToMany(targetEntity="app\model\Product", mappedBy="brand")
     */
    private $product;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getNameFantasy(): ?string
    {
        return $this->nameFantasy;
    }

    /**
     * @param string|null $nameFantasy
     * @return Brand
     */
    public function setNameFantasy(?string $nameFantasy): Brand
    {
        $this->nameFantasy = $nameFantasy;
        return $this;
    }

    /**
     * @return Person|null
     */
    public function getPerson(): ?Person
    {
        return $this->person;
    }

    /**
     * @param Person $person
     * @return Brand
     */
    public function setPerson(Person $person): Brand
    {
        $this->person = $person;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProduct(): ?Collection
    {
        return $this->product;
    }
}