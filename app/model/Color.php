<?php

namespace app\model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Color - Cor
 *
 * @ORM\Table(name="colors")
 * @ORM\Entity(repositoryClass="app\repository\Color\ColorRepository")
 */
class Color extends Model
{
    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", unique=true, nullable=false)
     *
     * @Assert\NotBlank(message="Campo descrição obrigatório")
     */
    private $description;

    /**
     * @var Collection|null - Produto
     *
     * @ORM\OneToMany(targetEntity="app\model\Product", mappedBy="color")
     */
    private $product;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Color
     */
    public function setDescription(string $description): Color
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getProduct(): ?Collection
    {
        return $this->product;
    }
}