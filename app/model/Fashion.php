<?php

namespace app\model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Fashion - Moda
 *
 * @ORM\Table(name="fashions")
 * @ORM\Entity(repositoryClass="app\repository\Fashion\FashionRepository")
 */
class Fashion extends Model
{
    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="string", unique=true, nullable=false)
     *
     * @Assert\NotBlank(message="Campo descrição obrigatório")
     */
    private $description;

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Fashion
     */
    public function setDescription(string $description): Fashion
    {
        $this->description = $description;
        return $this;
    }
}