<?php

namespace app\model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

/**
 * Class Sale - Venda
 *
 * @ORM\Table(name="sales")
 * @ORM\Entity(repositoryClass="app\repository\Sale\SaleRepository")
 */
class Sale extends Model
{
    /**
     * @var float $valueAll
     *
     * @ORM\Column(name="value_all", type="decimal", precision=10, scale=2, nullable=false)
     *
     * @Assert\NotNull(message="Campo Valor Total obrigatório")
     */
    private $valueAll;

    /**
     * @var boolean $pay
     *
     * @ORM\Column(name="pay", type="boolean", nullable=false)
     *
     * @Assert\NotNull(message="Campo estar pago obrigatório")
     */
    private $pay = false;

    /**
     * @var Client|null - Cliente
     *
     * @ORM\ManyToOne(targetEntity="app\model\Client", inversedBy="sale")
     * @ORM\JoinColumn(name="client", referencedColumnName="id", nullable=true)
     */
    private $client = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var Portion|null - Parcela
     *
     * @ORM\OneToOne(targetEntity="app\model\Portion", mappedBy="sale")
     */
    private $portion;
    /**
     * @var Collection|null - Venda Item
     *
     * @ORM\OneToMany(targetEntity="app\model\SaleItem", mappedBy="sale")
     */
    private $saleItem;

    public function __construct()
    {
        $this->saleItem = new ArrayCollection();
    }

    /**
     * @return float|null
     */
    public function getValueAll(): ?float
    {
        return $this->valueAll;
    }

    /**
     * @param float $valueAll
     * @return Sale
     */
    public function setValueAll(float $valueAll): Sale
    {
        $this->valueAll = $valueAll;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isPay(): ?bool
    {
        return $this->pay;
    }

    /**
     * @param bool $pay
     * @return Sale
     */
    public function setPay(bool $pay): Sale
    {
        $this->pay = $pay;
        return $this;
    }

    /**
     * @return Client|null
     */
    public function getClient(): ?Client
    {
        return $this->client;
    }

    /**
     * @param Client|null $client
     * @return Sale
     */
    public function setClient(?Client $client): Sale
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return Sale
     */
    public function setCreatedAt(DateTime $createdAt): Sale
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @return Portion|null
     */
    public function getPortion(): ?Portion
    {
        return $this->portion;
    }

    /**
     * @return Collection|null
     */
    public function getSaleItem(): ?Collection
    {
        return $this->saleItem;
    }
}