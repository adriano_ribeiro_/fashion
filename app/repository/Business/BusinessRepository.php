<?php

namespace app\repository\Business;

use app\repository\AbstractRepository;
use Throwable;

/**
 * Class BusinessRepository
 * @package app\repository\Business
 */
class BusinessRepository extends AbstractRepository implements BusinessRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->find($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}