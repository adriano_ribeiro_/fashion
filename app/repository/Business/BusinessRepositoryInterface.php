<?php

namespace app\repository\Business;

use app\repository\Brand\BrandRepositoryInterface;

/**
 * Interface BusinessRepositoryInterface
 * @package app\repository\Business
 */
interface BusinessRepositoryInterface extends BrandRepositoryInterface
{

}