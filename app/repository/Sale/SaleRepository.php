<?php

namespace app\repository\Sale;

use app\repository\AbstractRepository;

/**
 * Class SaleRepository
 * @package app\repository\Sale
 */
class SaleRepository extends AbstractRepository implements SaleRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}