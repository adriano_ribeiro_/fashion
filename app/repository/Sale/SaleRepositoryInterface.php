<?php

namespace app\repository\Sale;

use app\repository\RepositoryInterface;

/**
 * Interface SaleRepositoryInterface
 * @package app\repository\Sale
 */
interface SaleRepositoryInterface extends RepositoryInterface
{

}