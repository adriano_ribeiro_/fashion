<?php

namespace app\repository;

/**
 * Interface RepositoryInterface
 * @package app\repository
 */
interface RepositoryInterface
{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function findByDetails(int $id);
}