<?php

namespace app\repository\Product;

use app\model\Brand;
use app\model\Color;
use app\model\SizeProduct;
use app\repository\AbstractRepository;
use Throwable;

/**
 * Class ProductRepository
 * @package app\repository\Product
 */
class ProductRepository extends AbstractRepository implements ProductRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->createQueryBuilder('p')
                    ->select('p.id, p.description, p.reference, p.priceBuy, p.priceSale, p.quantity, b.nameFantasy as brand, sp.size, c.description as color')
                    ->innerJoin(Brand::class, 'b', 'WITH', 'b.id = p.brand')
                    ->innerJoin(SizeProduct::class, 'sp', 'WITH', 'sp.id = p.sizeProduct')
                    ->innerJoin(Color::class, 'c', 'WITH', 'c.id = p.color')
                    ->getQuery()
                    ->getResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        try {
            return $this->createQueryBuilder('p')
                    ->select('p.id, p.description, p.reference, p.priceBuy, p.priceSale, p.quantity, b.nameFantasy as brand, sp.size, c.description as color')
                    ->innerJoin(Brand::class, 'b', 'WITH', 'b.id = p.brand')
                    ->innerJoin(SizeProduct::class, 'sp', 'WITH', 'sp.id = p.sizeProduct')
                    ->innerJoin(Color::class, 'c', 'WITH', 'c.id = p.color')
                    ->where('p.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}