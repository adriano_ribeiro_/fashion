<?php

namespace app\repository\Product;

use app\repository\RepositoryInterface;

/**
 * Class ProductRepositoryInterface
 * @package app\repository\Product
 */
interface ProductRepositoryInterface extends RepositoryInterface
{

}