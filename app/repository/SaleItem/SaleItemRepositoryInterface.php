<?php

namespace app\repository\SaleItem;

use app\repository\RepositoryInterface;

/**
 * Interface SaleItemRepositoryInterface
 * @package app\repository\SaleItem
 */
interface SaleItemRepositoryInterface extends RepositoryInterface
{

}