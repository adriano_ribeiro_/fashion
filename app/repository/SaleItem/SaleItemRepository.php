<?php

namespace app\repository\SaleItem;

use app\repository\AbstractRepository;

/**
 * Class SaleItemRepository
 * @package app\repository\SaleItem
 */
class SaleItemRepository extends AbstractRepository implements SaleItemRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}