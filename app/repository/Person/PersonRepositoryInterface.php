<?php

namespace app\repository\Person;

use app\repository\RepositoryInterface;

/**
 * Interface PersonRepositoryInterface
 * @package app\repository\Person
 */
interface PersonRepositoryInterface extends RepositoryInterface
{

}