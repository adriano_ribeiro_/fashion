<?php

namespace app\repository\Person;

use app\repository\AbstractRepository;

/**
 * Class PersonRepository
 * @package app\repository\Person
 */
class PersonRepository extends AbstractRepository implements PersonRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}