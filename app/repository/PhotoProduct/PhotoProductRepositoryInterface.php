<?php

namespace app\repository\PhotoProduct;

use app\repository\RepositoryInterface;

/**
 * Interface PhotoProductRepositoryInterface
 * @package app\repository\PhotoProduct
 */
interface PhotoProductRepositoryInterface extends RepositoryInterface
{

}