<?php

namespace app\repository\PhotoProduct;

use app\repository\AbstractRepository;

/**
 * Class PhotoProductRepository
 * @package app\repository\PhotoProduct
 */
class PhotoProductRepository extends AbstractRepository implements PhotoProductRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}