<?php

namespace app\repository\User;

use app\repository\AbstractRepository;

/**
 * Class UserRepository
 * @package app\repository\User
 */
class UserRepository extends AbstractRepository implements UserRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}