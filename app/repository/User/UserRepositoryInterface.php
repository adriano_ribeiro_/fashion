<?php

namespace app\repository\User;

use app\repository\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package app\repository\User
 */
interface UserRepositoryInterface extends RepositoryInterface
{

}