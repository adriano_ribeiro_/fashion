<?php

namespace app\repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\UnitOfWork;
use Throwable;

/**
 * Class AbstractRepository
 * @package app\repository
 */
abstract class AbstractRepository extends EntityRepository
{
    /**
     * @param int $id
     * @param null $class
     * @return bool|object|null
     */
    public function getReference(int $id, $class = null)
    {
        try {
            if (!$class) {
                $class = $this->getClassName();
            }
            return $this->getEntityManager()->getReference($class, $id);
        } catch (ORMException $e) {
            throw new $e;
        }
    }

    /**
     * @param $entity
     */
    public function save($entity)
    {
        try {
            if ($this->getEntityManager()->getUnitOfWork()->getEntityState($entity) == UnitOfWork::STATE_NEW) {
                $this->getEntityManager()->persist($entity);
            }
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new $e;
        }
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
        try {
            $entity = $this->getReference($id);
            $this->getEntityManager()->remove($entity);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new $e;
        }
    }

    /**
     * @param string $name
     * @param null $class
     * @return object|null
     */
    public function findByUniqueName(string $name, $class = null)
    {
        try {
            if (!$class) {
                $class = $this->getClassName();
            }
            $object = $this->getEntityManager()->getRepository($class);
            return $object->findOneBy(['name' => $name]);
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}