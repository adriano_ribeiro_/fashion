<?php

namespace app\repository\Brand;

use app\repository\RepositoryInterface;

/**
 * Interface BrandRepositoryInterface
 * @package app\repository\Brand
 */
interface BrandRepositoryInterface extends RepositoryInterface
{

}