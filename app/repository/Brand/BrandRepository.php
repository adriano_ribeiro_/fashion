<?php

namespace app\repository\Brand;

use app\model\Person;
use app\repository\AbstractRepository;
use Throwable;

/**
 * Class BrandRepository
 * @package app\repository\Brand
 */
class BrandRepository extends AbstractRepository implements BrandRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->find($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->createQueryBuilder('b')
                    ->select('b.id, b.nameFantasy, p.name, p.phone')
                    ->innerJoin(Person::class, 'p', 'WITH', 'p.id = b.person')
                    ->groupBy('b.id')
                    ->getQuery()
                    ->getResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(int $id)
    {
        try {
            return $this->createQueryBuilder('b')
                        ->select('b.id, b.nameFantasy, p.name, p.phone')
                        ->innerJoin(Person::class, 'p', 'WITH', 'p.id = b.person')
                        ->where('b.id = :id')
                        ->setParameter('id', $id)
                        ->getQuery()
                        ->getOneOrNullResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}