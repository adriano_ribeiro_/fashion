<?php

namespace app\repository\SizeProduct;

use app\repository\RepositoryInterface;

/**
 * Class SizeProductRepositoryInterface
 * @package app\repository\SizeProduct
 */
interface SizeProductRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $size
     * @return mixed
     */
    public function findBySize(string $size);
}