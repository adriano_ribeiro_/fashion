<?php

namespace app\repository\SizeProduct;

use app\repository\AbstractRepository;
use Throwable;

/**
 * Class SizeProductRepository
 * @package app\repository\SizeProduct
 */
class SizeProductRepository extends AbstractRepository implements SizeProductRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        return $this->createQueryBuilder('s')
            ->select('s.id, s.size')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->find($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    public function findByDetails(int $id)
    {
        try {
            return $this->createQueryBuilder('s')
                ->select('s.id, s.size')
                ->where('s.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findBySize(string $size)
    {
        try {
            return $this->findOneBy(['size' => $size]);
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}