<?php

namespace app\repository\Fashion;

use app\repository\AbstractRepository;

/**
 * Class FashionRepository
 * @package app\repository\Fashion
 */
class FashionRepository extends AbstractRepository implements FashionRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}