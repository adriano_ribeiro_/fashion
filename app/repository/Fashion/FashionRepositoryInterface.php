<?php

namespace app\repository\Fashion;

use app\repository\RepositoryInterface;

/**
 * Interface FashionRepositoryInterface
 * @package app\repository\Fashion
 */
interface FashionRepositoryInterface extends RepositoryInterface
{

}