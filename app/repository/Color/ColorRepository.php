<?php

namespace app\repository\Color;

use app\repository\AbstractRepository;
use Throwable;

/**
 * Class ColorRepository
 * @package app\repository\Color
 */
class ColorRepository extends AbstractRepository implements ColorRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->find($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c.id, c.description')
                ->getQuery()
                ->getResult()
            ;
        } catch(Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findByDescription(string $description)
    {
        try {
            return $this->findOneBy(['description' => $description]);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(int $id)
    {
        try {
            return $this->createQueryBuilder('c')
                    ->select('c.id, c.description')
                    ->where('c.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult()
            ;
        } catch (Throwable $e) {
            throw new $e;
        }
    }
}