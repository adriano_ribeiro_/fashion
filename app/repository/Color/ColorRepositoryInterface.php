<?php

namespace app\repository\Color;

use app\repository\RepositoryInterface;

/**
 * Interface ColorRepositoryInterface
 * @package app\repository\Color
 */
interface ColorRepositoryInterface extends RepositoryInterface
{
    /**
     * @param string $description
     * @return mixed
     */
    public function findByDescription(string $description);
}