<?php

namespace app\repository\Client;

use app\repository\AbstractRepository;
use Throwable;

/**
 * Class ClientRepository
 * @package app\repository\Client
 */
class ClientRepository extends AbstractRepository implements ClientRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->find($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}