<?php

namespace app\repository\Client;

use app\repository\RepositoryInterface;

/**
 * Interface ClientRepositoryInterface
 * @package app\repository\Client
 */
interface ClientRepositoryInterface extends RepositoryInterface
{
    
}