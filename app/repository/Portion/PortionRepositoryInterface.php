<?php

namespace app\repository\Portion;

use app\repository\RepositoryInterface;

/**
 * Interface PortionRepositoryInterface
 * @package app\repository\Portion
 */
interface PortionRepositoryInterface extends RepositoryInterface
{

}