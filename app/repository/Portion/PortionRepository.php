<?php

namespace app\repository\Portion;

use app\repository\AbstractRepository;

/**
 * Class PortionRepository
 * @package app\repository\Portion
 */
class PortionRepository extends AbstractRepository implements PortionRepositoryInterface
{

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}