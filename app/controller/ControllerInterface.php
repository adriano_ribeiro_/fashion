<?php

namespace app\controller;

use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Interface ControllerInterface
 * @package app\controller
 */
interface ControllerInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request);

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param Request $request
     * @return mixed
     */
    public function findById(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function findByDetails(Request $request);
}