<?php

namespace app\controller;

use app\service\Product\ProductServiceInterface;
use app\traits\JsonTrait;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;

/**
 * Class ProductController
 * @package app\controller
 */
class ProductController implements ControllerInterface
{
    use JsonTrait;

    /** @var ProductServiceInterface */
    private $service;

    /**
     * ProductController constructor.
     * @param ProductServiceInterface $service
     */
    public function __construct(ProductServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @inheritDoc
     */
    public function save(Request $request)
    {
        try {
            return $this->service->save([$request->getParsedBody(), $request->getUploadedFiles()['files']]);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function update(Request $request)
    {
        // TODO: Implement update() method.
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->service->all();
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(Request $request)
    {
        // TODO: Implement findById() method.
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(Request $request)
    {
        try {
            return $this->service->findByDetails($request->getAttribute('id'));
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}