<?php

namespace app\controller;

use app\service\Brand\BrandServiceInterface;
use app\traits\JsonTrait;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;

/**
 * Class BrandController
 * @package app\controller
 */
class BrandController implements ControllerInterface
{
    use JsonTrait;

    /** @var BrandServiceInterface */
    private $service;

    /**
     * BrandController constructor.
     * @param BrandServiceInterface $service
     */
    public function __construct(BrandServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @inheritDoc
     */
    public function save(Request $request)
    {
        try {
            return $this->service->save($request->getParsedBody());
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function update(Request $request)
    {
        try {
            return $this->service->update($request->getAttribute('id'), $request->getParsedBody());
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->service->all();
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(Request $request)
    {
        try {
            return $this->service->findById($request->getAttribute('id'));
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function findByDetails(Request $request)
    {
        try {
            return $this->service->findByDetails($request->getAttribute('id'));
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @return mixed|MessageInterface
     */
    public function selectBrand()
    {
        try {
            return $this->service->selectBrand();
        } catch (Throwable $e) {
            return $this->toJson($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}