<?php

namespace app\controller;

use app\service\Color\ColorServiceInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;

/**
 * Class ColorController
 * @package app\controller
 */
class ColorController implements ControllerInterface
{
    /** @var ColorServiceInterface */
    private $service;

    /**
     * ColorController constructor.
     * @param ColorServiceInterface $service
     */
    public function __construct(ColorServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     * @inheritDoc
     */
    public function save(Request $request)
    {
        try {
            return $this->service->save($request->getParsedBody());
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function update(Request $request)
    {
        try {
            return $this->service->update($request->getAttribute('id'), $request->getParsedBody());
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->service->all();
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(Request $request)
    {
        try {
            return $this->service->findById($request->getAttribute('id'));
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function findByDetails(Request $request)
    {
        try {
            return $this->service->findByDetails($request->getAttribute('id'));
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }

    /**
     * @return mixed
     */
    public function selectColor()
    {
        try {
            return $this->service->selectColor();
        } catch (Throwable $e) {
            dd($e->getMessage());
        }
    }
}