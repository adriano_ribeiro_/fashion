<?php

namespace app\traits;

use app\exception\ValidationException;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Symfony\Component\Validator\Validation;

/**
 * Trait ValidatorTrait
 * @package app\traits
 */
trait ValidatorTrait
{
    /**
     * @var array
     */
    private $errors = [];

    /**
     * @param $class
     * @throws ValidationException
     */
    public function valid($class)
    {
        AnnotationRegistry::registerLoader('class_exists');
        $validateClass = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $errors = $validateClass->validate($class);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                //$this->errors[] = $error->getPropertyPath() . ": " . $error->getMessage() . ". ";
                $this->errors[] = $error->getMessage();
            }
            $string = implode(" ", $this->errors);
            throw new ValidationException($string, 9999);
        }
    }
}