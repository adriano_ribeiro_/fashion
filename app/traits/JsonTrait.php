<?php

namespace app\traits;

use Psr\Http\Message\MessageInterface;
use Slim\Psr7\Response;

/**
 * Trait JsonTrait
 * @package app\traits
 */
trait JsonTrait
{
    /** @var bool  */
    protected $codeSuccess = true;

    /** @var bool  */
    protected $codeError = false;

    /** @var int  */
    protected $codeWarning = 2;

    /** @var int  */
    protected $codeInfo = 1;

    /** @var string  */
    protected $messageSuccess = 'SUCESSO ';

    /** @var string  */
    protected $messageError = 'ERRO ';

    /** @var string  */
    protected $messageInfo = 'INFORMAÇÃO ';

    /** @var string  */
    protected $messageWarning = 'ADVERTÊNCIA ';

    /** @var string  */
    protected $validation = 'VALIDAÇÃO ';


    /**
     * @param $code
     * @param $message
     * @param $data
     * @return false|string
     */
    public function success ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return false|string
     */
    public function error ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return false|string
     */
    public function info ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return false|string
     */
    public function warning ($code, $message, $data)
    {
        return $this->toJson($code ,$message, $data);
    }

    /**
     * @param $code
     * @param $message
     * @param $data
     * @return MessageInterface
     */
    private function toJson($code, $message, $data): MessageInterface
    {
        $response = new Response();

        $json = [
            'code' => $code,
            'message' => $message,
            'data' => $data
        ];

        $payload = json_encode($json);

        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    }
}