<?php

namespace app\traits;

use DateTime;
use Exception;

/**
 * Trait FieldsGeneric
 * @package app\traits
 */
trait FieldsGeneric
{
    /**
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @return DateTime|null
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * TODO: pre-persist -> antes de inserir a entidade no banco de dados
     *
     * @ORM\PrePersist
     * @return $this
     * @throws Exception
     */
    public function setCreatedAt()
    {
        $this->createdAt = new DateTime();
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     * @return $this
     * @throws Exception
     */
    public function setUpdatedAt()
    {
        $this->updatedAt = new DateTime();
        return $this;
    }
}