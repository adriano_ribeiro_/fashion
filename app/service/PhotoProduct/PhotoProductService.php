<?php

namespace app\service\PhotoProduct;

use app\exception\ValidationException;
use app\factory\PhotoProductFactory;
use app\model\PhotoProduct;
use app\repository\PhotoProduct\PhotoProductRepositoryInterface;
use app\traits\JsonTrait;
use app\traits\ValidatorTrait;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

/**
 * Class PhotoProductService
 * @package app\service\PhotoProduct
 */
class PhotoProductService implements PhotoProductServiceInterface
{
    use ValidatorTrait;
    use JsonTrait;

    /** @var PhotoProduct */
    private $photoProduct;

    /** @var PhotoProductRepositoryInterface */
    private $photoProductRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PhotoProductService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->photoProductRepository = $em->getRepository(PhotoProduct::class);
    }

    /**
     * @inheritDoc
     * @throws ValidationException
     * @throws Throwable
     */
    public function save(array $data)
    {
        try {
           $this->photoProduct = PhotoProductFactory::build($data);
           $this->valid($this->photoProduct);
           $this->photoProductRepository->save($this->photoProduct);
           return $this->photoProduct;
        } catch (ValidationException $e) {
            throw $e;
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->toJson($this->codeSuccess, $this->messageSuccess, $this->photoProductRepository->all());
        } catch (Throwable $e) {

        }
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}