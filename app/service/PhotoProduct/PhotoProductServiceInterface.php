<?php

namespace app\service\PhotoProduct;

use app\service\ServiceInterface;

/**
 * Interface PhotoProductServiceInterface
 * @package app\service\PhotoProduct
 */
interface PhotoProductServiceInterface extends ServiceInterface
{

}