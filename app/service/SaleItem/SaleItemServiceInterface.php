<?php

namespace app\service\SaleItem;

use app\service\ServiceInterface;

/**
 * Interface SaleItemServiceInterface
 * @package app\service\SaleItem
 */
interface SaleItemServiceInterface extends ServiceInterface
{

}