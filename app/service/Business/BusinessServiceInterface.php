<?php

namespace app\service\Business;

use app\service\ServiceInterface;

/**
 * Interface BusinessServiceInterface
 * @package app\service\Business
 */
interface BusinessServiceInterface extends ServiceInterface
{

}