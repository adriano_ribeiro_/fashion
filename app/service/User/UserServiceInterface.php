<?php

namespace app\service\User;

use app\service\ServiceInterface;

/**
 * Interface UserServiceInterface
 * @package app\service\User
 */
interface UserServiceInterface extends ServiceInterface
{

}