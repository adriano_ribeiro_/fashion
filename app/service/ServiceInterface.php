<?php

namespace app\service;

/**
 * Interface ServiceInterface
 * @package app\service
 */
interface ServiceInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public function save(array $data);

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, array $data);

    /**
     * @return mixed
     */
    public function all();

    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id);

    /**
     * @param int $id
     * @return mixed
     */
    public function findByDetails(int $id);
}