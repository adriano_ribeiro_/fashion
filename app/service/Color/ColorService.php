<?php

namespace app\service\Color;

use app\exception\ValidationException;
use app\factory\ColorFactory;
use app\model\Color;
use app\repository\Color\ColorRepositoryInterface;
use app\traits\JsonTrait;
use app\traits\ValidatorTrait;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

/**
 * Class ColorService
 * @package app\service\Color
 */
class ColorService implements ColorServiceInterface
{
    use ValidatorTrait;

    use JsonTrait;

    /** @var Color */
    private $color;

    /** @var ColorRepositoryInterface */
    private $colorRepository;

    /** @var EntityManagerInterface */
    private $em;

    /**
     * ColorService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->colorRepository = $this->em->getRepository(Color::class);
    }

    /**
     * @inheritDoc
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->color = $this->colorRepository->findByDescription($data['description']);
            if ($this->color) {
                return $this->warning($this->codeWarning, $this->messageWarning, 'Essa cor ' . $data['description'] . ' já existe.');
            }
            $this->color = ColorFactory::build($data);
            $this->valid($this->color);
            $this->colorRepository->save($this->color);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->color->getDescription());
        } catch (ValidationException $e) {
            $this->em->rollback();
            throw new $e;
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->color = $this->colorRepository->getReference($id);

            if ($this->color->getDescription() !== $data['description']) {
                if ($this->colorRepository->findByDescription($data['description'])) {
                    return $this->warning($this->codeWarning, $this->messageWarning, 'Cor ' . $data['description'] . ' já existe.');
                }
            }

            $this->valid($this->color->setDescription($data['description']));
            $this->colorRepository->save($this->color);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->color->getDescription());
        } catch (ValidationException $e) {
            $this->em->rollback();
            throw new $e;
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->colorRepository->all());
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->colorRepository->findById($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(int $id)
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->colorRepository->findByDetails($id));
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function selectColor()
    {
        try {
            $colors = $this->colorRepository->all();
            $array = array();
            foreach ($colors as $product) {
                $data = array(
                    "id" => $product['id'],
                    "text" => $product['description']
                );
                array_push($array, $data);
            }
            return $this->success($this->codeSuccess, $this->messageSuccess, $array);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}