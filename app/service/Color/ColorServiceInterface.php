<?php

namespace app\service\Color;

use app\service\ServiceInterface;

/**
 * Interface ColorServiceInterface
 * @package app\service\Color
 */
interface ColorServiceInterface extends ServiceInterface
{
    /**
     * @return mixed
     */
    public function selectColor();
}