<?php

namespace app\service\Fashion;

use app\service\ServiceInterface;

/**
 * Interface FashionServiceInterface
 * @package app\service\Fashion
 */
interface FashionServiceInterface extends ServiceInterface
{

}