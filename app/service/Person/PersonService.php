<?php

namespace app\service\Person;

use app\exception\ValidationException;
use app\factory\PersonFactory;
use app\model\Person;
use app\repository\Person\PersonRepositoryInterface;
use app\traits\ValidatorTrait;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PersonService
 * @package app\service\Person
 */
class PersonService implements PersonServiceInterface
{
    use ValidatorTrait;

    /** @var Person */
    private $person;

    /** @var PersonRepositoryInterface */
    private $personRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->personRepository = $this->em->getRepository(Person::class);
    }

    /**
     * @inheritDoc
     */
    public function save(array $data)
    {
        try {
            $this->person = PersonFactory::build($data);
            $this->valid($this->person);
            $this->personRepository->save($this->person);
            return $this->person;
        } catch (ValidationException $e) {
            throw new $e;
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data)
    {
        try {
            $this->person = $this->personRepository->getReference($id);

            $this->valid($this->person
                ->setName($data['name'])
                ->setPhone($data['phone'])
            );
            $this->personRepository->save($this->person);
            return $this->person;
        } catch (ValidationException $e) {
            throw new $e;
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(int $id)
    {
        // TODO: Implement findByDetails() method.
    }
}