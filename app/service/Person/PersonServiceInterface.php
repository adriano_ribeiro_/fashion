<?php

namespace app\service\Person;

use app\service\ServiceInterface;

/**
 * Interface PersonServiceInterface
 * @package app\service\Person
 */
interface PersonServiceInterface extends ServiceInterface
{

}