<?php

namespace app\service\Client;

use app\service\ServiceInterface;

/**
 * Interface ClientServiceInterface
 * @package app\service\Client
 */
interface ClientServiceInterface extends ServiceInterface
{

}