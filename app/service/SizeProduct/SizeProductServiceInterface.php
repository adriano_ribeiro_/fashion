<?php

namespace app\service\SizeProduct;

use app\service\ServiceInterface;

/**
 * Interface SizeProductServiceInterface
 * @package app\service\SizeProduct
 */
interface SizeProductServiceInterface extends ServiceInterface
{
    /**
     * @return mixed
     */
    public function selectSizeProduct();
}