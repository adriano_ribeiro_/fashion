<?php

namespace app\service\SizeProduct;

use app\exception\ValidationException;
use app\factory\SizeProductFactory;
use app\model\SizeProduct;
use app\repository\SizeProduct\SizeProductRepositoryInterface;
use app\traits\JsonTrait;
use app\traits\ValidatorTrait;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SizeProductService
 * @package app\service\SizeProduct
 */
class SizeProductService implements SizeProductServiceInterface
{
    use ValidatorTrait;

    use JsonTrait;

    /** @var SizeProduct */
    private $sizeProduct;

    /** @var SizeProductRepositoryInterface */
    private $sizeProductRepository;

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->sizeProductRepository = $this->em->getRepository(SizeProduct::class);
    }

    /**
     * @inheritDoc
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->sizeProduct = $this->sizeProductRepository->findBySize($data['size']);
            if ($this->sizeProduct) {
                return $this->warning($this->codeWarning, $this->messageWarning, 'Tamanho ' . $data['size'] . ' já existe.');
            }
            $this->sizeProduct = SizeProductFactory::build($data);
            $this->valid($this->sizeProduct);
            $this->sizeProductRepository->save($this->sizeProduct);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->sizeProduct->getSize());
        } catch (ValidationException $e) {
            $this->em->rollback();
            throw new $e;
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->sizeProduct = $this->sizeProductRepository->getReference($id);

            if ($this->sizeProduct->getSize() !== $data['size']) {
                if ($this->sizeProductRepository->findBySize($data['size'])) {
                    return $this->warning($this->codeWarning, $this->messageWarning, 'Tamanho ' . $data['size'] . ' já existe.');
                }
            }

            $this->valid($this->sizeProduct->setSize($data['size']));
            $this->sizeProductRepository->save($this->sizeProduct);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->sizeProduct->getSize());
        } catch (ValidationException $e) {
            $this->em->rollback();
            throw new $e;
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->sizeProductRepository->all());
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->sizeProductRepository->findById($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(int $id)
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->sizeProductRepository->findByDetails($id));
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function selectSizeProduct()
    {
        try {
            $sizeProducts = $this->sizeProductRepository->all();
            $array = array();
            foreach ($sizeProducts as $sizeProduct) {
                $data = array(
                    "id" => $sizeProduct['id'],
                    "text" => $sizeProduct['size']
                );
                array_push($array, $data);
            }
            return $this->success($this->codeSuccess, $this->messageSuccess, $array);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}