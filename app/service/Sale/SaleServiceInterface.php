<?php

namespace app\service\Sale;

use app\service\ServiceInterface;

/**
 * Interface SaleServiceInterface
 * @package app\service\Sale
 */
interface SaleServiceInterface extends ServiceInterface
{

}