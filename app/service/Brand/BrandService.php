<?php

namespace app\service\Brand;

use app\exception\ValidationException;
use app\factory\BrandFactory;
use app\model\Brand;
use app\model\Person;
use app\repository\Brand\BrandRepositoryInterface;
use app\repository\Person\PersonRepositoryInterface;
use app\service\Person\PersonServiceInterface;
use app\traits\JsonTrait;
use app\traits\ValidatorTrait;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class BrandService
 * @package app\service\Brand
 */
class BrandService implements BrandServiceInterface
{
    use ValidatorTrait;

    use JsonTrait;

    /** @var Brand */
    private $brand;

    /** @var BrandRepositoryInterface */
    private $brandRepository;

    /** @var PersonRepositoryInterface */
    private $personRepository;

    /** @var PersonServiceInterface */
    private $personService;

    /** @var EntityManagerInterface */
    private $em;

    /**
     * BrandService constructor.
     * @param EntityManagerInterface $em
     * @param PersonServiceInterface $personService
     */
    public function __construct(EntityManagerInterface $em, PersonServiceInterface $personService)
    {
        $this->em = $em;

        $this->personService = $personService;

        $this->brandRepository = $this->em->getRepository(Brand::class);
        $this->personRepository = $this->em->getRepository(Person::class);
    }

    /**
     * @inheritDoc
     */
    public function save(array $data)
    {
        try {
            if ($this->personRepository->findByUniqueName($data['name'])) {
                return $this->warning($this->codeWarning, $this->messageWarning, 'Marca ' . $data['name'] . ' já existe.');
            }
            $this->em->beginTransaction();
            $person = $this->personService->save($data);
            $this->brand = BrandFactory::build(array_merge($data, ['person' => $person]));
            $this->valid($this->brand);
            $this->brandRepository->save($this->brand);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->brand->getPerson()->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            return $this->warning($this->codeWarning, $this->messageWarning, $e->getMessage());
        } catch (Throwable $e) {
            $this->em->rollback();
            return $this->error($this->codeError, $e->getMessage(), false);
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->brand = $this->brandRepository->getReference($id);

            if ($this->brand->getPerson()->getName() !== $data['name']) {
                if ($this->personRepository->findByUniqueName($data['name'])) {
                    return $this->warning($this->codeWarning, $this->messageWarning, 'Marca ' . $data['description'] . ' já existe.');
                }
            }
            /** @var Person $person */
            $person = $this->personService->update($this->brand->getPerson()->getId(), $data);

            $this->valid($this->brand
                ->setPerson($person)
                ->setNameFantasy($data['nameFantasy'])
            );

            $this->brandRepository->save($this->brand);
            $this->em->commit();
            return $this->success($this->codeSuccess, $this->messageSuccess, $person->getName());
        } catch (ValidationException $e) {
            $this->em->rollback();
            throw new $e;
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->brandRepository->all());
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        try {
            return $this->brandRepository->findById($id);
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findByDetails(int $id)
    {
        try {
            return $this->success($this->codeSuccess, $this->messageSuccess, $this->brandRepository->findByDetails($id));
        } catch (Throwable $e) {
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function selectBrand()
    {
        try {
            $products = $this->brandRepository->all();
            $array = array();
            foreach ($products as $product) {
                $data = array(
                    "id" => $product['id'],
                    "text" => $product['nameFantasy']
                );
                array_push($array, $data);
            }
            return $this->success($this->codeSuccess, $this->messageSuccess, $array);
        } catch (Throwable $e) {
            return $this->error($this->codeError, $this->messageError, $e->getMessage());
        }
    }
}