<?php

namespace app\service\Brand;

use app\service\ServiceInterface;

/**
 * Interface BrandServiceInterface
 * @package app\service\Brand
 */
interface BrandServiceInterface extends ServiceInterface
{
    /**
     * @return mixed
     */
    public function selectBrand();
}