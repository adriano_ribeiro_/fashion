<?php

namespace app\service\Product;

use app\service\ServiceInterface;
use Slim\Psr7\UploadedFile;

/**
 * Interface ProductServiceInterface
 * @package app\service\Product
 */
interface ProductServiceInterface extends ServiceInterface
{

    /**
     * @param array $data
     * @param UploadedFile[]
     * @return mixed
     */
    public function saveWithFiles(array $data, array $uploadFile);

}