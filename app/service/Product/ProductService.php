<?php

namespace app\service\Product;

use app\factory\PhotoProductFactory;
use app\factory\ProductFactory;
use app\model\Brand;
use app\model\Color;
use app\model\PhotoProduct;
use app\model\Product;
use app\model\SizeProduct;
use app\repository\Brand\BrandRepositoryInterface;
use app\repository\Color\ColorRepositoryInterface;
use app\repository\PhotoProduct\PhotoProductRepositoryInterface;
use app\repository\Product\ProductRepositoryInterface;
use app\repository\SizeProduct\SizeProductRepositoryInterface;
use app\service\PhotoProduct\PhotoProductServiceInterface;
use app\traits\JsonTrait;
use app\traits\ValidatorTrait;
use Doctrine\ORM\EntityManagerInterface;
use Slim\Psr7\UploadedFile;
use Throwable;

/**
 * Class ProductService
 * @package app\service\Product
 */
class ProductService implements ProductServiceInterface
{
    use ValidatorTrait;
    use JsonTrait;

    /** @var Product */
    private $product;

    /** @var PhotoProduct */
    private $photoProduct;

    /** @var Brand */
    private $brand;

    /** @var SizeProduct */
    private $sizeProduct;

    /** @var Color */
    private $color;

    /** @var ProductRepositoryInterface */
    private $productRepository;

    /** @var PhotoProductRepositoryInterface */
    private $photoProductRepository;

    /** @var PhotoProductServiceInterface */
    private $photoProductService;

    /** @var BrandRepositoryInterface */
    private $brandRepository;

    /** @var SizeProductRepositoryInterface */
    private $sizeProductRepository;

    /** @var ColorRepositoryInterface */
    private $colorRepository;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductService constructor.
     * @param EntityManagerInterface $em
     * @param PhotoProductServiceInterface $photoProductService
     */
    public function __construct(
        EntityManagerInterface $em,
        PhotoProductServiceInterface $photoProductService
    )
    {
        $this->em = $em;
        $this->photoProductService = $photoProductService;

        $this->productRepository = $this->em->getRepository(Product::class);
        $this->photoProductRepository = $this->em->getRepository(PhotoProduct::class);
        $this->brandRepository = $this->em->getRepository(Brand::class);
        $this->sizeProductRepository = $this->em->getRepository(SizeProduct::class);
        $this->colorRepository = $this->em->getRepository(Color::class);
    }

    /**
     * @inheritDoc
     */
    public function save(array $data)
    {
        try {
            $this->em->beginTransaction();
            $this->brand = $this->brandRepository->getReference($data[0]['brand']);
            $this->sizeProduct = $this->sizeProductRepository->getReference($data[0]['sizeProduct']);
            $this->color = $this->colorRepository->getReference($data[0]['color']);

            $this->product = ProductFactory::build(array_merge($data[0],
                ['brand' => $this->brand, 'sizeProduct' => $this->sizeProduct, 'color' => $this->color])
            );
            $this->valid($this->product);
            $this->productRepository->save($this->product);
            if (isset($data[1])) {
                foreach ($data[1] as $file) {
                    $this->photoProductService->save(array_merge(['photoProduct' => $file], ['product' => $this->product]));
                }
            }
            $this->em->commit();
            return $this->toJson($this->codeSuccess, $this->messageSuccess, $this->product->getDescription());
        } catch (Throwable $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }

    /**
     * @inheritDoc
     * @throws Throwable
     */
    public function all()
    {
        try {
            return $this->toJson($this->codeSuccess, $this->messageSuccess, $this->productRepository->all());
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id)
    {
        // TODO: Implement findById() method.
    }

    /**
     * @inheritDoc
     * @throws Throwable
     */
    public function findByDetails(int $id)
    {
        try {
            return $this->toJson($this->codeSuccess, $this->messageSuccess, $this->productRepository->findByDetails($id));
        } catch (Throwable $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @param UploadedFile[]
     * @return mixed
     */
    public function saveWithFiles(array $data, array $uploadFile)
    {
        $data1 = $data;
        $uploadFile1 = $uploadFile;
        dd(
            $data1,
            $uploadFile1
        );
    }
}