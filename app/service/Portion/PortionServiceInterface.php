<?php

namespace app\service\Portion;

use app\service\ServiceInterface;

/**
 * Interface PortionServiceInterface
 * @package app\service\Portion
 */
interface PortionServiceInterface extends ServiceInterface
{

}