<?php

namespace app\view;

use Psr\Http\Message\ResponseInterface;

/**
 * Class SizeProductView
 * @package app\view
 */
class SizeProductView extends ContainerView
{
    /**
     * HomeView constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render('size_product/size_product.html.twig');
    }
}