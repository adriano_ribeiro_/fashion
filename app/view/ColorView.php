<?php

namespace app\view;

use Psr\Http\Message\ResponseInterface;

/**
 * Class HomeView
 * @package app\view
 */
class ColorView extends ContainerView
{
    /**
     * HomeView constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render('color/colors.html.twig');
    }
}