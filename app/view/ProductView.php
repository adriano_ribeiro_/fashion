<?php

namespace app\view;

use Psr\Http\Message\ResponseInterface;

/**
 * Class ProductView
 * @package app\view
 */
class ProductView extends ContainerView
{
    /**
     * HomeView constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render('product/products.html.twig');
    }
}