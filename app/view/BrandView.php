<?php

namespace app\view;

use Psr\Http\Message\ResponseInterface;

/**
 * Class BrandView
 * @package app\view
 */
class BrandView extends ContainerView
{
    /**
     * HomeView constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return ResponseInterface
     */
    public function index()
    {
        return $this->render('brand/brands.html.twig');
    }
}