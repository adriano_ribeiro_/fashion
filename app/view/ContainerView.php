<?php

namespace app\view;

use app\provider\View;

/**
 * Class ContainerView
 * @package app\view
 */
abstract class ContainerView extends View
{
}