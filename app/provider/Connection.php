<?php

namespace app\provider;

use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;

/**
 * Class Connection
 * @package app\provider
 */
class Connection
{
    /**
     * @return EntityManagerInterface
     * @throws ORMException
     */
    public static function conn(): EntityManagerInterface
    {
        $paths = array(base_path('app/model'));
        $isDevMode = true;

//        $dbParams = [
//            'driver' => 'pdo_mysql',
//            'host' => 'sql169.main-hosting.eu',
//            'port' => '3306',
//            'user' => 'u967327326_root',
//            'password' => '112233',
//            'dbname' => 'u967327326_moda_dev',
//            'charset' => 'utf8'
//        ];

        $dbParams = [
            'driver' => 'pdo_mysql',
            'host' => '127.0.0.1',
            'port' => '3306',
            'user' => 'root',
            'password' => '2710',
            'dbname' => 'fashion',
            'charset' => 'utf8'
        ];

        try {
            $cache = new ArrayCache();
            $config = new Configuration();
            $config->setMetadataCacheImpl($cache);
            $config = Setup::createConfiguration($isDevMode);
            AnnotationRegistry::registerLoader('class_exists');
            $driverImpl = $config->newDefaultAnnotationDriver($paths);
            $config->setMetadataDriverImpl($driverImpl);
            $config->setQueryCacheImpl($cache);
            $config->setProxyDir(base_path('app/proxy'));
            $config->setProxyNamespace(base_path('app/proxy'));
            $config->setAutoGenerateProxyClasses(true);
            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, base_path('app/proxy'), $cache, false);
            return EntityManager::create($dbParams, $config);
        } catch (ORMException $e) {
            throw $e;
        }
    }
}