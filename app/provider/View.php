<?php

namespace app\provider;

use app\util\TwigFunctions;
use Psr\Http\Message\ResponseInterface;
use Slim\Psr7\Response;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extra\Intl\IntlExtension;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

/**
 * Class View
 * @package app\provider
 */
abstract class View
{
    /**
     * @var Environment
     */
    protected $twig;

    /**
     * View constructor.
     */
    public function __construct()
    {
        $loader = new FilesystemLoader(base_path('resources/views'));
        $twig = new Environment($loader);

        $twigFunctions = new TwigFunction(\TwigFunctions::class, function ($method, $params = []) {
            return TwigFunctions::$method($params);
        });
        $twig->addFunction($twigFunctions);
        $twig->addExtension(new IntlExtension());

        $this->twig = $twig;
    }

    /**
     * @param string $view
     * @param array $data
     * @return ResponseInterface
     */
    public function render(string $view, array $data = []): ResponseInterface
    {
        $response = new Response();
        try {
            $response->getBody()->write(
                $this->twig->render($view, $data)
            );
            return $response;
        } catch (LoaderError $e) {
            throw new $e;
        } catch (RuntimeError $e) {
            throw new $e;
        } catch (SyntaxError $e) {
            throw new $e;
        }
    }
}