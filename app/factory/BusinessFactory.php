<?php

namespace app\factory;

use app\model\Business;

/**
 * Class BusinessFactory
 * @package app\factory
 */
class BusinessFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Business|mixed
     */
    public static function build(array $data)
    {
        return (new Business())
            ->setPhoto($data['photo'])
            ->setPerson($data['person'])
        ;
    }
}