<?php

namespace app\factory;

use app\model\SizeProduct;

/**
 * Class SaleItemFactory
 * @package app\factory
 */
class SizeProductFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return SizeProduct|mixed
     */
    public static function build(array $data)
    {
        return (new SizeProduct())
            ->setSize($data['size'])
        ;
    }
}