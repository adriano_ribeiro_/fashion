<?php

namespace app\factory;

/**
 * Interface FactoryInterface
 * @package app\factory
 */
interface FactoryInterface
{
    /**
     * @param array $data
     * @return mixed
     */
    public static function build(array $data);
}