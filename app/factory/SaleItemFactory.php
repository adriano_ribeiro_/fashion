<?php

namespace app\factory;

use app\model\SaleItem;

/**
 * Class SaleItemFactory
 * @package app\factory
 */
class SaleItemFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return SaleItem|mixed
     */
    public static function build(array $data)
    {
        return (new SaleItem())
            ->setProduct($data['product'])
            ->setQuantity($data['quantity'])
            ->setSubtotal($data['subtotal'])
            ->setSale($data['sale'])
        ;
    }
}