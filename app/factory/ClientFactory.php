<?php

namespace app\factory;

use app\model\Client;

/**
 * Class ClientFactory
 * @package app\factory
 */
class ClientFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Client|mixed
     */
    public static function build(array $data)
    {
        return (new Client())
            ->setPerson($data['person'])
        ;
    }
}