<?php

namespace app\factory;

use app\model\Brand;
use app\model\Person;

/**
 * Class PersonFactory
 * @package app\factory
 */
class PersonFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Brand|mixed
     */
    public static function build(array $data)
    {
        return (new Person())
            ->setName($data['name'])
            ->setPhone($data['phone'])
        ;
    }
}