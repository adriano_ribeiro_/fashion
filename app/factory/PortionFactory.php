<?php

namespace app\factory;

use app\model\Portion;

/**
 * Class PortionFactory
 * @package app\factory
 */
class PortionFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return mixed
     */
    public static function build(array $data)
    {
        return (new Portion())
            ->setValue($data['value'])
            ->setSale($data['sale'])
            ->setPayDay($data['payDay'])
        ;
    }
}