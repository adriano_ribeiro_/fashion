<?php

namespace app\factory;

use app\model\PhotoProduct;
use app\model\Product;
use Slim\Psr7\UploadedFile;

/**
 * Class PhotoProductFactory
 * @package app\factory
 */
class PhotoProductFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Product|mixed
     */
    public static function build(array $data)
    {
        /** @var UploadedFile $file */
        $file = $data['photoProduct'];
        # Encode the image string data into base64
        $base64 = base64_encode($file->getClientFilename());
        $photoProduct = new PhotoProduct();
        return $photoProduct
            ->setBase64($base64)
            ->setProduct($data['product'])
        ;
    }
}