<?php

namespace app\factory;

use app\model\User;

/**
 * Class SaleItemFactory
 * @package app\factory
 */
class UserFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return User|mixed
     */
    public static function build(array $data)
    {
        return (new User())
            ->setLogin($data['login'])
            ->setPassword($data['password'])
            ->setPerson($data['person'])
        ;
    }
}