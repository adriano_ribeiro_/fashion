<?php

namespace app\factory;

use app\model\Sale;

/**
 * Class SaleFactory
 * @package app\factory
 */
class SaleFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Sale|mixed
     */
    public static function build(array $data)
    {
        return (new Sale())
            ->setValueAll($data['valueAll'])
            ->setPay($data['pay'])
            ->setClient($data['client'])
            ->setCreatedAt($data['createdAt'])
        ;
    }
}