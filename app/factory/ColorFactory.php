<?php

namespace app\factory;

use app\model\Color;

/**
 * Class ColorFactory
 * @package app\factory
 */
class ColorFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Color|mixed
     */
    public static function build(array $data)
    {
        return (new Color())
            ->setDescription($data['description'])
        ;
    }
}