<?php

namespace app\factory;

use app\model\Brand;

/**
 * Class BrandFactory
 * @package app\factory
 */
class BrandFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Brand|mixed
     */
    public static function build(array $data)
    {
        return (new Brand())
            ->setNameFantasy($data['nameFantasy'])
            ->setPerson($data['person'])
        ;
    }
}