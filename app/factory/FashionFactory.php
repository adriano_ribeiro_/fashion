<?php

namespace app\factory;

use app\model\Fashion;

/**
 * Class FashionFactory
 * @package app\factory
 */
class FashionFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Fashion|mixed
     */
    public static function build(array $data)
    {
        return (new Fashion())
            ->setDescription($data['description'])
        ;
    }
}