<?php

namespace app\factory;

use app\model\Product;
use app\util\Mask;

/**
 * Class ProductFactory
 * @package app\factory
 */
class ProductFactory implements FactoryInterface
{

    /**
     * @param array $data
     * @return Product|mixed
     */
    public static function build(array $data)
    {
        return (new Product())
            ->setDescription($data['description'])
            ->setReference($data['reference'])
            ->setPriceBuy(Mask::convertStringToFloat($data['priceBuy']))
            ->setPriceSale(Mask::convertStringToFloat($data['priceSale']))
            ->setQuantity($data['quantity'])
            ->setBrand($data['brand'])
            ->setSizeProduct($data['sizeProduct'])
            ->setColor($data['color'])
        ;
    }
}