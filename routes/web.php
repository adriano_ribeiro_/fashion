<?php

use app\controller\BrandController;
use app\controller\ColorController;
use app\controller\ProductController;
use app\controller\SizeProductController;
use app\view\BrandView;
use app\view\ColorView;
use app\view\ProductView;
use app\view\SizeProductView;
use Slim\Factory\AppFactory;
use Slim\Interfaces\RouteCollectorProxyInterface;

if (session_id() == '' || !isset($_SESSION)) {
    session_start();
}

date_default_timezone_set('America/Fortaleza');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Content-Type");

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require __DIR__ . '/../bootstrap/container.php';

try {
    AppFactory::setContainer($containerBuilder->build());
    $app = AppFactory::create();

    # rotas HomeView
    $app->group('', function (RouteCollectorProxyInterface $route) {
        $route->get('/', 'app\view\HomeView:index');
    });

    # rotas ColorView
    $app->group('/cores', function (RouteCollectorProxyInterface $route) {
        $route->get('', ColorView::class.':index');
    });

    # rotas ColorController
    $app->group('/color', function (RouteCollectorProxyInterface $route) {
        $route->post('/create', ColorController::class.':save');
        $route->get('/details/{id}', ColorController::class.':findByDetails');
        $route->post('/update/{id}', ColorController::class.':update');
        $route->get('/all', ColorController::class.':all');
        $route->get('/select', ColorController::class.':selectColor');
    });

    # rotas BrandView
    $app->group('/marcas', function (RouteCollectorProxyInterface $route) {
        $route->get('', BrandView::class.':index');
    });

    # rotas BrandController
    $app->group('/brand', function (RouteCollectorProxyInterface $route) {
        $route->post('/create', BrandController::class.':save');
        $route->get('/details/{id}', BrandController::class.':findByDetails');
        $route->post('/update/{id}', BrandController::class.':update');
        $route->get('/all', BrandController::class.':all');
        $route->get('/select', BrandController::class.':selectBrand');
    });

    # rotas BrandView
    $app->group('/tamanhos', function (RouteCollectorProxyInterface $route) {
        $route->get('', SizeProductView::class.':index');
    });

    # rotas BrandController
    $app->group('/size', function (RouteCollectorProxyInterface $route) {
        $route->post('/product/create', SizeProductController::class.':save');
        $route->get('/product/details/{id}', SizeProductController::class.':findByDetails');
        $route->post('/product/update/{id}', SizeProductController::class.':update');
        $route->get('/product/all', SizeProductController::class.':all');
        $route->get('/select', SizeProductController::class.':selectSizeProduct');
    });

    # rotas ProductView
    $app->group('/produtos', function (RouteCollectorProxyInterface $route) {
        $route->get('', ProductView::class.':index');
    });

    # rotas ProductController
    $app->group('/product', function (RouteCollectorProxyInterface $route) {
        $route->post('/create', ProductController::class.':save');
        $route->get('/details/{id}', ProductController::class.':findByDetails');
        $route->post('/update/{id}', ProductController::class.':update');
        $route->get('/all', ProductController::class.':all');
    });

    $app->run();

} catch (Throwable $e) {
    dd($e);
}