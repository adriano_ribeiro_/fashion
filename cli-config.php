<?php

use app\provider\Connection;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

try {
    return ConsoleRunner::createHelperSet(Connection::conn());
} catch (ORMException $e) {
    throw new $e;
}
