<?php

namespace test\Model;

use app\factory\PhotoProductFactory;
use app\factory\ProductFactory;
use app\model\Brand;
use app\model\Color;
use app\model\PhotoProduct;
use app\model\Product;
use app\model\SizeProduct;
use app\provider\Connection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Throwable;

class ProductTest extends TestCase
{
    /**
     * @return string
     */
    public function testSave()
    {
        try {
            /** @var EntityManagerInterface $em */
            $em = Connection::conn();
            $em->beginTransaction();

            $brand = $em->getRepository(Brand::class)->findById(1);
            $sizeProduct = $em->getRepository(SizeProduct::class)->findById(1);
            $color = $em->getRepository(Color::class)->findById(1);

            $data = [
                'description' => 'Camisa',
                'reference' => '2020',
                'priceBuy' => 38.00,
                'priceSale' => 89.00,
                'quantity' => 5,
                'brand' => $brand,
                'sizeProduct' => $sizeProduct,
                'color' => $color,
            ];

            $product = ProductFactory::build($data);
            $em->persist($product);
            $this->assertInstanceOf(Product::class, $product);

            # Get the image and convert into string
            $img =
                'C:\fakepath\conjunto-infantil-roupa-para-meninos-de-1-a-3-anos-gravata.jpg';

            # Encode the image string data into base64
            $base64 = base64_encode($img);

            $data = [
                'base64' => $base64,
                'product' => $product
            ];

            if (isset($data['productPhoto'])) {
                $this->assertIsArray($data);
            }
            $photoProduct = PhotoProductFactory::build($data);
            $em->persist($photoProduct);
            $em->flush();
            $em->commit();
            $this->assertInstanceOf(PhotoProduct::class, $photoProduct);
        } catch (Throwable $e) {
            if ($em->getConnection()->isTransactionActive()) {
                $em->rollback();
            }
            $this->assertInstanceOf(Throwable::class, $e);
            return $e->getMessage();
        }
    }
}