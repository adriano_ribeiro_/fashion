<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 13/01/2020
 * Time: 21:26
 */

namespace test\Model;

use app\factory\BrandFactory;
use app\model\Brand;
use app\provider\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\TestCase;
use Throwable;

class BrandTest extends TestCase
{
    public function testSave()
    {

        try {
            /** @var EntityManager $em */
            $em = Connection::conn();

            $dados = [
                'nameFantasy' => 'nome fantasia 2',
                'name' => 'teste 2',
                'phone' => 'sdasdas 2'
            ];

            $brand = BrandFactory::build($dados);

            $em->persist($brand);
            $em->flush();
        } catch (Throwable $e) {
            dd($e);
        }
    }

    /**
     * @throws ORMException
     */
    public function testFindById()
    {
        /** @var EntityManager $em */
        $em = Connection::conn();

        $result = $em->getRepository(Brand::class)->findById(1);

        if ($result) {
            $this->assertInstanceOf(Brand::class, $result);
        } else {
            $this->assertNull($result);
        }
    }

    /**
     * @throws ORMException
     */
    public function testFindByDetails()
    {
        /** @var EntityManager $em */
        $em = Connection::conn();

        $result = $em->getRepository(Brand::class)->findByDetails(1);

        if ($result) {
            $this->assertIsArray($result);
        } else {
            $this->assertNull($result);
        }
    }
}