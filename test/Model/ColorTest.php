<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 13/01/2020
 * Time: 21:26
 */

namespace test\Model;

use app\factory\ColorFactory;
use app\model\Color;
use app\provider\Connection;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Throwable;

/**
 * Class ColorTest
 * @package test\Model
 */
class ColorTest extends TestCase
{
    /**
     * @throws Throwable
     */
    public function testSave()
    {
            /** @var EntityManager $em */
            $em = Connection::conn();

            $data = [
                'description' => 'Azul'
            ];

            $color = ColorFactory::build($data);

            $em->persist($color);
            $em->flush();
    }

    /**
     * @throws Throwable
     */
    public function testAll()
    {
        /** @var EntityManager $em */
        $em = Connection::conn();

        $result = $em->getRepository(Color::class)->all();

        if ($result) {
            $this->assertIsArray($result);
        } else {
            $this->assertEmpty($result);
        }
    }
}