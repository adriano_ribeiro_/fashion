<?php
/**
 * Created by PhpStorm.
 * User: jsj
 * Date: 13/01/2020
 * Time: 21:36
 */

namespace test;


use app\provider\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\SchemaTool;
use PHPUnit\Framework\TestCase;

class TestUnit extends TestCase
{

    protected $entityManager = null;

    /**
     * Executado antes de cada teste unitário
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\Tools\ToolsException
     */
    public function setup() : void
    {
        /** @var EntityManager $entityManager */
        $entityManager = Connection::conn();

        //Obtem informações das entidades que encontrar em Tableless\Entity
        $classes = $entityManager->getMetadataFactory()->getAllMetadata();

        $tool = new SchemaTool($entityManager);
        // Cria a base de dados necessária com suas determinadas tabelas
        $tool->createSchema($classes);

        parent::setup();
    }

    /**
     * Executado após a execução de cada um dos testes unitários
     * @throws \Doctrine\ORM\ORMException
     */
    public function tearDown() : void
    {
        $entityManager = Connection::conn();
        $tool = new SchemaTool($entityManager);

        //Obtem informações das entidades que encontrar em Tableless\Entity
        $classes = $entityManager->getMetadataFactory()->getAllMetadata();

        // Desfaz o banco criado no setUp
        $tool->dropSchema($classes);

        parent::tearDown();
    }

    /**
     *
     * @return EntityManagerInterface
     * @throws \Doctrine\ORM\ORMException
     */
    public function getEntityManager() : EntityManagerInterface
    {
        if (!$this->entityManager) {
            $this->entityManager = Connection::conn();
        }
        return $this->entityManager;
    }
}