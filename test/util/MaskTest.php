<?php

namespace test\util;

use app\util\Mask;
use PHPUnit\Framework\TestCase;

class MaskTest extends TestCase
{

    public function testConvertStringToFloat()
    {
        $value = '10,00';
        $result = Mask::convertStringToFloat($value);
        self::assertIsFloat($result);
    }
}