<?php

use app\controller\BrandController;
use app\controller\ColorController;
use app\controller\ProductController;
use app\controller\SizeProductController;
use app\provider\Connection;
use app\service\Brand\BrandService;
use app\service\Brand\BrandServiceInterface;
use app\service\Color\ColorService;
use app\service\Color\ColorServiceInterface;
use app\service\Person\PersonService;
use app\service\Person\PersonServiceInterface;
use app\service\PhotoProduct\PhotoProductService;
use app\service\PhotoProduct\PhotoProductServiceInterface;
use app\service\Product\ProductService;
use app\service\Product\ProductServiceInterface;
use app\service\SizeProduct\SizeProductService;
use app\service\SizeProduct\SizeProductServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use function Di\autowire;
use function DI\get;

return [

    EntityManagerInterface::class => function () {
        return Connection::conn();
    },

    ############################################################################################################

    PersonServiceInterface::class => autowire(PersonService::class)
        ->constructor(get(EntityManagerInterface::class)),

    ColorServiceInterface::class => autowire(ColorService::class)
        ->constructor(get(EntityManagerInterface::class)),

    ColorController::class => autowire(ColorController::class)
        ->constructor(get(ColorServiceInterface::class)),

    BrandServiceInterface::class => autowire(BrandService::class)
        ->constructor(
            get(EntityManagerInterface::class),
            get(PersonServiceInterface::class)
     ),

    BrandController::class => autowire(BrandController::class)
        ->constructor(get(BrandServiceInterface::class)),

    SizeProductServiceInterface::class => autowire(SizeProductService::class)
        ->constructor(get(EntityManagerInterface::class)),

    SizeProductController::class => autowire(SizeProductController::class)
        ->constructor(get(SizeProductServiceInterface::class)),

    PhotoProductServiceInterface::class => autowire(PhotoProductService::class)
        ->constructor(get(EntityManagerInterface::class)),

    ProductServiceInterface::class => autowire(ProductService::class)
    ->constructor(
        get(EntityManagerInterface::class),
        get(PhotoProductServiceInterface::class)
    ),

    ProductController::class => autowire(ProductController::class)
        ->constructor(get(ProductServiceInterface::class)),

];